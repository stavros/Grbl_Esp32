#pragma once
// clang-format off

#define MACHINE_NAME            "AGIS_LASER"

#define X_STEP_PIN              GPIO_NUM_13
#define X_DIRECTION_PIN         GPIO_NUM_16
#define Y_STEP_PIN              GPIO_NUM_17
#define Y_DIRECTION_PIN         GPIO_NUM_22

#define X_LIMIT_PIN             GPIO_NUM_26
#define Y_LIMIT_PIN             GPIO_NUM_27

/*
#define DEFAULT_HOMING_ENABLE 0
#define DEFAULT_HOMING_DIR_MASK 0 // move positive dir Z, negative X,Y
#define DEFAULT_HOMING_FEED_RATE 200.0 // mm/min
#define DEFAULT_HOMING_SEEK_RATE 1000.0 // mm/min
#define DEFAULT_HOMING_DEBOUNCE_DELAY 250 // msec (0-65k)
#define DEFAULT_HOMING_PULLOFF 3.0 // mm
*/

#define SPINDLE_TYPE              SpindleType::LASER
#define LASER_OUTPUT_PIN          GPIO_NUM_4
#define DEFAULT_LASER_MODE        1
#define DEFAULT_SPINDLE_RPM_MAX   1000.0 // can be change to your spindle max

#define DEFAULT_X_MAX_RATE      5000.0
#define DEFAULT_Y_MAX_RATE      5000.0

#define DEFAULT_X_ACCELERATION  50.0
#define DEFAULT_Y_ACCELERATION  50.0

#define DEFAULT_X_STEPS_PER_MM  100.0
#define DEFAULT_Y_STEPS_PER_MM  100.0

#define DEFAULT_X_MAX_TRAVEL    330.0
#define DEFAULT_Y_MAX_TRAVEL    225.0
