#pragma once

#define MACHINE_NAME            "TIMSAV"

#define STEPPERS_DISABLE_PIN GPIO_NUM_2
#define DEFAULT_INVERT_ST_ENABLE        0

#define X_STEP_PIN              GPIO_NUM_22
#define X_DIRECTION_PIN         GPIO_NUM_25

#define Y_STEP_PIN              GPIO_NUM_26
#define Y_DIRECTION_PIN         GPIO_NUM_27

#define USING_SERVO
#define Z_SERVO_PIN             GPIO_NUM_4

#define DEFAULT_Z_MAX_TRAVEL          5.0
#define DEFAULT_Z_HOMING_MPOS         5.0
#define Z_SERVO_CAL_MIN               1.0   // calibration factor for the minimum PWM duty
#define Z_SERVO_CAL_MAX               1.0   // calibration factor for the maximum PWM duty

#define SPINDLE_TYPE            SpindleType::BESC
#define SPINDLE_OUTPUT_PIN      GPIO_NUM_13

#define DEFAULT_X_MAX_RATE 5000.0
#define DEFAULT_Y_MAX_RATE DEFAULT_X_MAX_RATE

#define DEFAULT_X_ACCELERATION 50.0
#define DEFAULT_Y_ACCELERATION DEFAULT_X_ACCELERATION

#define DEFAULT_X_STEPS_PER_MM 100.0
#define DEFAULT_Y_STEPS_PER_MM DEFAULT_X_STEPS_PER_MM

#define DEFAULT_X_MAX_TRAVEL 300.0
#define DEFAULT_Y_MAX_TRAVEL DEFAULT_X_MAX_TRAVEL
